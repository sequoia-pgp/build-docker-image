#!/bin/bash
ARGS="$@"

# It is important to use `-l` (login shell) so that PATH is properly
# populated with mingw specific paths (e.g. `/mingw64/bin`)
/usr/bin/bash -lc "$ARGS"
