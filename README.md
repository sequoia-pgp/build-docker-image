Builds a docker image suitable for running our tests in Gitlab's
continuous integration setup.

The images are built in two steps.  First, a base image is created
with the necessary toolchain and build dependencies.  Then, the base
image is used to build Sequoia, hence pre-populating the cargo and
target directories for faster builds.

In pipelines related to merge requests, the images are built and
stored in the Docker Registry as `name:${CI_COMMIT_SHA}`.  Then, if
the merge request is merged into the main branch, a tag `name:latest`
is created, committing the change so that it is used by the CI jobs.
