FROM mcr.microsoft.com/dotnet/framework/runtime:4.8-windowsservercore-ltsc2019

SHELL ["powershell", "-NonInteractive"]

# Install rustup
RUN cd $env:TMP \
  ; Invoke-WebRequest "https://win.rustup.rs/x86_64" \
    -OutFile "rustup-init.exe" \
  ; .\rustup-init.exe -y \
    --default-toolchain 1.79.0 \
    --default-host x86_64-pc-windows-gnu \
  ; Remove-Item rustup-init.exe

# Install Cap’n Proto.
RUN cd $env:TMP \
  ; Invoke-WebRequest "https://capnproto.org/capnproto-c++-win32-0.10.3.zip" \
    -OutFile "capnp.zip" \
  ; Expand-Archive "capnp.zip" -DestinationPath C:\capnp \
  ; Remove-Item "capnp.zip" \
  ; setx path \"$env:PATH;C:\capnp\capnproto-tools-win32-0.10.3\" /m

# Install chocolatey
RUN Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install MSYS2/mingw-w64
# NOTE: We don't perform msys core update as this hangs after finishing,
# possibly related to lingering GnuPG daemon, e.g.
# https://github.com/microsoft/vcpkg/pull/11810.
RUN choco install -y --no-progress git msys2 --params "/NoUpdate"

ADD entrypoint-mingw.sh .
SHELL ["C:\\tools\\msys64\\usr\\bin\\env", "MSYSTEM=MINGW64", "CHERE_INVOKING=1", "MSYS2_PATH_TYPE=inherit", "./entrypoint-mingw.sh"]

# To speed up CI jobs we already preinstall these dependencies at image build
# time, since MSYS mirrors are often very slow and/or the CI runner is slow.

ADD clean_package_cache.hook C:/tools/msys64/etc/pacman.d/hooks/clean_package_cache.hook

RUN pacman -S --noconfirm --refresh --needed \
    base-devel \
    git \
    gnupg \
    mingw-w64-x86_64-gcc \
    mingw-w64-x86_64-clang \
    mingw-w64-x86_64-pkg-config \
    mingw-w64-x86_64-bzip2 \
    mingw-w64-x86_64-nettle \
    mingw-w64-x86_64-sqlite3 \
    mingw-w64-x86_64-capnproto

# Use a wrapper script to quote-surround any additional arguments passed by the
# user, as expected by the `/usr/bin/bash -c "..."` form.
ENTRYPOINT ["C:\\tools\\msys64\\usr\\bin\\env", "MSYSTEM=MINGW64", "CHERE_INVOKING=1", "MSYS2_PATH_TYPE=inherit", "./entrypoint-mingw.sh"]
CMD ["bash"]
